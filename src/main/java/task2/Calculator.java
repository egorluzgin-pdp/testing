package task2;

import java.math.BigInteger;

public class Calculator {

    private Calculator() {
        throw new AssertionError("Suppress default constructor for noninstantiability");
    }

    public static int addition(int first, int second) {
        return (int) addition((float) first, (float) second);
    }

    public static float addition(float first, float second) {
        return first + second;
    }

    public static int subtraction(int first, int second) {
        return (int) subtraction((float) first, (float) second);
    }

    public static float subtraction(float first, float second) {
        return first - second;
    }

    public static int multiplication(int first, int second) {
        return (int) multiplication((float) first, (float) second);
    }

    public static float multiplication(float first, float second) {
        return first * second;
    }

    public static float division(int first, int second) {
        return division((float) first, (float) second);
    }

    public static float division(float first, float second) {
        if (second == 0f) {
            throw new IllegalArgumentException("Division by zero");
        }
        return first / second;
    }

    public static double root(int value) {
        return root((float) value);
    }

    public static double root(float value) {
        if (value < 0) {
            throw new IllegalArgumentException("Value should be greatest than zero");
        }
        return Math.sqrt(value);
    }

    public static double pow(int value, int power) {
        return Math.pow(value, power);
    }

    public static int fibonacciSequence(int n) {
        if (n <= 1) {
            return n;
        }
        return fibonacciSequence(n - 1) + fibonacciSequence(n - 2);
    }

    public static boolean isPrime(long value) {
        BigInteger bigInteger = BigInteger.valueOf(value);
        return bigInteger.isProbablePrime((int) Math.log(value));
    }

}
