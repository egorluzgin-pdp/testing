package task3.service;

import task3.entity.User;

public interface UserManagerService {

    User createNewUser(String login, String password);

    User updateUser(User user);

    void removeUser(User user);

    User findById(Long id);
}
