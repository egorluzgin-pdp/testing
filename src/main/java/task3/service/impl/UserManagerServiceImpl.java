package task3.service.impl;

import task3.dao.UserManagerDao;
import task3.entity.User;
import task3.service.UserManagerService;

import java.time.LocalDateTime;

public class UserManagerServiceImpl implements UserManagerService {

    private UserManagerDao dao;

    @Override
    public User createNewUser(String login, String password) {
        User newUser = getNewUser(login, password);
        return dao.createOrUpdate(newUser);
    }

    private User getNewUser(String login, String password) {
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        user.setCreateDate(LocalDateTime.now());
        return user;
    }

    @Override
    public User updateUser(User user) {
        return dao.createOrUpdate(user);
    }

    @Override
    public void removeUser(User user) {
        dao.remove(user.getId());
    }

    @Override
    public User findById(Long id) {
        return dao.findUserById(id);
    }
}
