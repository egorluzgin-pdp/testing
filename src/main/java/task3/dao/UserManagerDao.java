package task3.dao;

import task3.entity.User;

public interface UserManagerDao {

    User createOrUpdate(User user);

    User findUserById(Long id);

    void remove(Long id);

}
