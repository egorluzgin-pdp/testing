package task1;

import org.apache.commons.lang3.StringUtils;

import java.util.stream.IntStream;

public final class Utils {

    private Utils() {
        throw new AssertionError("Suppress default constructor for noninstantiability");
    }

    public static String concatenateWords(String... words) {
        return StringUtils.join(words);
    }

    public static int computeFactorial(int f) {
        if (f < 0) {
            throw new IllegalArgumentException("Value: " + f + " must be equals or greater than zero");
        }
        return IntStream.rangeClosed(2, f).reduce((x, y) -> x * y).orElse(1);
    }

}
