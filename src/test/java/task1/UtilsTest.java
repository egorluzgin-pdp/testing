package task1;

import org.junit.Ignore;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;

public class UtilsTest {

    private static final String FIRST_WORD = "First";
    private static final String SECOND_WORD = "Second";

    @Test
    public void testConcatenateWords() {
        String result = Utils.concatenateWords(FIRST_WORD, SECOND_WORD);

        assertEquals(FIRST_WORD + SECOND_WORD, result);
    }

    @Ignore
    @Test
    public void testNormalizeWord() {
        String result = Utils.concatenateWords(FIRST_WORD);

        assertEquals(FIRST_WORD, result);
    }

    @Test
    public void testComputeFactorial() {
        int result = Utils.computeFactorial(5);

        assertEquals(120, result);
    }

    @Test
    public void testComputeFactorialWithZeroValue() {
        int result = Utils.computeFactorial(0);

        assertEquals(1, result);
    }

    @Test(timeout = 1)
    public void testFactorialWithTimeout() {
        Random random = new Random();
        Utils.computeFactorial(random.nextInt());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExpectedException() {
        Utils.computeFactorial(-5);
    }
}