package task3.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import task3.dao.UserManagerDao;
import task3.entity.User;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserManagerServiceImplTest {

    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final long USER_ID = 15L;

    @InjectMocks
    private UserManagerServiceImpl service;

    @Mock
    private UserManagerDao dao;

    @Captor
    private ArgumentCaptor<User> userCaptor;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void createNewUser() {
        service.createNewUser(LOGIN, PASSWORD);

        verify(dao).createOrUpdate(userCaptor.capture());
        User user = userCaptor.getValue();

        assertEquals(LOGIN, user.getLogin());
        assertEquals(PASSWORD, user.getPassword());
        assertNotNull(user.getCreateDate());
        verify(dao).createOrUpdate(user);
    }

    @Test
    public void updateUser() {
        User user = getUser();
        service.updateUser(user);

        verify(dao).createOrUpdate(user);
    }

    @Test
    public void removeUser() {
        User user = getUser();
        service.removeUser(user);

        verify(dao).remove(USER_ID);
    }

    private User getUser() {
        User user = new User();
        user.setId(USER_ID);
        return user;
    }

    @Test
    public void findById() {
        User user = getUser();
        when(dao.findUserById(USER_ID)).thenReturn(user);

        User result = service.findById(USER_ID);

        assertEquals(USER_ID, (long) result.getId());
    }
}