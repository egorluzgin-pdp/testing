package task2;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

public class CalculatorPositiveTest {

    @Test
    public void testAdditionInt() {
        assertEquals(11, Calculator.addition(5, 6));
    }

    @Test
    public void testAdditionFloat() {
        assertEquals(12.1f, Calculator.addition(5.5f, 6.6f), 0.0001);
    }

    @Test
    public void testSubtractionInt() {
        assertEquals(-1, Calculator.subtraction(5, 6));
    }

    @Test
    public void testSubtractionFloat() {
        assertEquals(-1f, Calculator.subtraction(5f, 6f), 0.0001);
    }

    @Test
    public void testMultiplicationInt() {
        assertEquals(30, Calculator.multiplication(5, 6));
    }

    @Test
    public void testMultiplicationFloat() {
        assertEquals(30f, Calculator.multiplication(5f, 6f), 0.0001);
    }

    @Test
    public void testDivisionInt() {
        assertEquals(1.5f, Calculator.division(3, 2), 0.0001);
    }

    @Test
    public void testDivisionFloat() {
        assertEquals(1.5f, Calculator.division(3f, 2f), 0.0001);
    }

    @Test
    public void testRootInt() {
        assertEquals(3.0, Calculator.root(9), 0.0001);
    }

    @Test
    public void testRootFloat() {
        assertEquals(3.0, Calculator.root(9f), 0.0001);
    }

    @Test
    public void testPow() {
        assertEquals(531441.0, Calculator.pow(9, 6), 0.001);
    }

    @Test
    public void testFibonacciSequenceValueIsZero() {
        int result = Calculator.fibonacciSequence(0);

        assertThat(result, equalTo(0));
    }

    @Test
    public void testFibonacciSequenceValueIsOne() {
        int result = Calculator.fibonacciSequence(1);

        assertThat(result, equalTo(1));
    }

    @Test
    public void testFibonacciSequenceValue() {
        int result = Calculator.fibonacciSequence(15);

        assertThat(result, equalTo(610));
    }

    @Test
    public void testIsPrimeIntTrue() {
        assertTrue(Calculator.isPrime(3));
    }

    @Test
    public void testIsPrimeIntFalse() {
        assertFalse(Calculator.isPrime(4));
    }

}