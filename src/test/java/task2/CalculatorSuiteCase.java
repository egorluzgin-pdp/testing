package task2;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({CalculatorPositiveTest.class, CalculatorNegativeTest.class} )
public class CalculatorSuiteCase {
}
