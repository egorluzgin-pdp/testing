package task2;

import org.junit.Test;

public class CalculatorNegativeTest {

    @Test(expected = IllegalArgumentException.class)
    public void testDivisionByZero() {
        Calculator.division(3f, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRootValueLessThanZero() {
        Calculator.root(-1);
    }

}